"use strict";


const form = document.body.querySelector('.password-form');
const btn = document.body.querySelector('.btn');
const [input1, input2] = document.body.querySelectorAll('.password-input');

form.addEventListener('click', (e) => showPassword(e));
form.addEventListener('input', (e) => hideHint(e));
btn.addEventListener('click', () => comparePassword());

const hint = document.createElement('span');
hint.classList.add("hint");

const showPassword = (e) => {
    if (e.target.classList.contains("fa-eye-slash")) {
        e.target.classList.remove("fa-eye-slash");
        e.target.previousElementSibling.setAttribute("type", "password");
    } else if (e.target.classList.contains("fa-eye")) {
        e.target.classList.add('fa-eye-slash');
        e.target.previousElementSibling.setAttribute("type", "text");
    }
}

const hideHint = (e) => {
    if (!!e.target.value) hint.textContent = "";
    btn.removeAttribute("disabled");
}

const showHint = () => {
    hint.textContent = "* Потрібно ввести однакові значення";
    form.children[1].insertAdjacentElement("beforeend", hint);
}

const showModal = () => {
    const modal = document.createElement('div');
    modal.classList.add('modal');
    modal.textContent = "You are welcome!"
    document.body.append(modal);
    setTimeout(() => modal.remove(), 2000);
}

const comparePassword = () => {
    const isEqual = !!input1.value.trim() && input1.value === input2.value;
    if (isEqual) {
        form.remove();
        showModal();
    } else showHint();

    [input1, input2].forEach(input => input.value = '');
}
